'use strict';

var Packet = function(blocks){
  this.blocks = blocks;
};

Packet.prototype.serialize = function(){
  var blocks = this.blocks.map(function(b){ return b.serialize(); });
  var dataLength = blocks.reduce(function(a, e){ return a + e.length; }, 0);
  var buffer = new Buffer(dataLength + 3);
  var ptr = 1;
  buffer[0] = 0x7F;
  blocks.forEach(function(b){
    b.copy(buffer, ptr);
    ptr += b.length;
  });
  buffer[ptr++] = 0xFF;
  buffer[ptr++] = 0x0A;
  return buffer;
};

exports.build = function(blocks){
  return new Packet(blocks).serialize();
};

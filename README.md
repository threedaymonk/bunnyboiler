# Bunny Boiler

Nabaztag:tag hacking.

## Get started

Set up your rabbit in configuration mode: unplug your Nabaztag:tag, hold down
the button on its head, and plug it in again. Let go as soon as the lights turn
blue.

* Connect to the Nabaztag's wireless network (NabaztagXX, where XX is the last
  two digits of its MAC address).
* Visit the configuration page at http://192.168.0.1/.
* Under *Advanced configuration*, check that the firmware version is 0.0.0.10
* Set *Violet platform* to `http://host:8111/vl` where host is the IP address
  of the computer running bunnyboiler.
* Click on *Update settings* and wait for the rabbit to reboot.

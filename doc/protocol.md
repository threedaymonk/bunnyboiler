# Nabaztag:tag server communication

This information was originally at
[www.cs.uta.fi](http://www.cs.uta.fi/hci/spi/jnabserver/documentation/index.html)
but has since disappeared from there. It has been reformatted and edited in
places to improve readability.

## Hardware

The Nabaztag:tag has five LEDs, four in front and one in bottom. There are two
moving ears whose positions are tracked with optical sensors, a microphone, a
speaker, and an RFID-reader.

The RFID-reader reads ISO 14443 B tags. These tags operate on 13.56MHz and
contain a 4096-bit EEPROM. The Nabaztag can read the IDs of these tags.

Sounds recorded from the microphone are encoded with ADPCM before they are sent
to the server.

## Bootcode

When the Nabaztag connects to the server for the first time after being powered
on or rebooted, it requests bootcode from the server. The bootcode contains most
of what the Nabaztag needs to operate: logic for responding to user events and
instructions from the server.

## Protocol

On basic network configuration Nabaztag is working as a HTTP network client
which automatically calls the server at a fixed interval: this is the ping call.
The ping interval is variable and can be changed by sending PingIntervalBlock in the
packet.

The Nabaztag uses five resources: `bc.jsp`, `locate.jsp`, `p4.jsp`,
`record.jsp` and `rfid.jsp`. These calls are hardcoded in the byte code.

The Nabaztag's serial number is passed in some requests. This is the digits of
the MAC address, i.e. without colons between the octets.

## Requests

### GET [Violet platform]/bc.jsp

Get bootcode.

|Parameter|Description                |
|---------|---------------------------|
|v        |Nabaztag version           |
|m        |Nabaztag MAC address       |
|l        |Unknown MAC address (zeros)|
|p        |Unknown MAC address (zeros)|
|h        |Unknown                    |

After booting the Nabaztag requests `bc.jsp` from the "Violet platform" server
specified in the device configuration. It will repeat until this succeeds.

### GET [Violet platform]/locate.jsp

Get service locations.

|Parameter|Description        |
|---------|-------------------|
|sn       |Nabaztag serial no.|
|h        |Unknown            |

When Nabaztag has successfully loaded the byte code it calls `locate.jsp`
to obtain the hosts of the *ping* and *broadcast* servers. The response is thus:

    ping example.com\nbroad example.com

Port numbers are optional, but can be supplied in the normal way, e.g.
`example.com:1234`.

The first URL is the ping server on which `p4.jsp` will be called.

The second URL is the broadcast server. The Nabaztag will replace all
"broadcast" strings from the message block with this value when it recieves
a message.

### GET [ping server]/vl/p4.jsp

Ping server: request messages.

|Parameter|Description                          |
|---------|-------------------------------------|
|sn       |Nabaztag serial no.                  |
|v        |Version of loaded bootcode           |
|st       |TODO                                 |
|sd       |Last action user has made on Nabaztag|
|tc       |TODO                                 |
|h        |Unknown                              |

The purpose of ping is to tell the server if any input has come from the user
and whether the last message has been carried out.

User input is described in the `sd` parameter:

|Value of sd|Meaning                      |
|-----------|-----------------------------|
|0          |Server timed out             |
|1          |User has clicked twice       |
|2          |Message received and executed|
|3          |User has clicked once        |
|8xy        |Ear positions (in x and y)   |

When ear positions are received, `sd` is three characters long, and consists of
8 followed by values for both ears.

### POST [Violet platform]/vl/record.jsp

Upload audio recording.

|Parameter|Description               |
|---------|--------------------------|
|sn       |Nabaztag serial no.       |
|v        |Version of loaded bootcode|
|h        |Unknown                   |
|m        |Unknown                   |

When the button on the Nabaztag's head is pressed, it starts recording audio.
Once the button is released, the encoded audio data is POSTed to the server.

### GET [Violet platform]/vl/rfid.jsp

RFID tag scanned.

|Parameter|Description               |
|---------|--------------------------|
|sn       |Nabaztag serial no.       |
|v        |Version of loaded bootcode|
|h        |Unknown                   |
|t        |ID of sniffed tag         |

If the Nabaztag fails to read the ID, or if the ID is kept in front of the
sensor so that it repeatedly reads the tag, the Nabaztag sends an ID of sixteen
zeros.

## Packets

The server can send a packet to the Nabaztag in response to a ping request.

An answer packet consists of `7F`, followed by any number of blocks, and
finishes with `FF 0A`.

A block has three parts:

* type: one byte
* length: three bytes, most significant first.
* data: *length* bytes

Below is an example of a packet containing two blocks. The first block is type
`03`, has a length of one, and the value of its only byte is five. The next
block is type `09` and has a length of zero.

    |  |  :        :  |  :        |     |
     7F 03 00 00 01 05 09 00 00 00 FF 0A
    |  |  :        :  |  :        |     |

## Blocks

### Ping interval block (`03`)

A ping interval contains one byte of data. When this block is sent to the
Nabaztag it changes the ping interval to the given time in seconds.

### Ambient block (`04`)

An ambient block sets light flashing patterns when there is no other messages
playing. These lights can be interrupted by making the Nabaztag perform
another operation such as playing a sound.

An ambient block is always 22 to 24 bytes long. The length of the block
determines how the nose LED blinks:

|Length|Blink pattern|
|------|-------------|
|22    |Off          |
|23    |Single 1/s   |
|24    |Double 1/s   |

Bytes 21 and 22 set the positions of the left and right ears respectively.

The block data always starts with bytes `7F FF FF FF`. Values of bytes from
5 to 20 are unknown. These can be filled with zeros.

### Reboot block (`09`)

The reboot block is always `09 00 00 00`. This reboots Nabaztag and causes it
to reload its bootcode.

### Message block (`0A`)

A message block is an obfuscated ASCII string message sent to Nabaztag. The obfuscated
byte array `C` is generated from the plain text `P` with this algorithm:

    INV8 = [
      0x01, 0xAB, 0xCD, 0xB7, 0x39, 0xA3, 0xC5, 0xEF,
      0xF1, 0x1B, 0x3D, 0xA7, 0x29, 0x13, 0x35, 0xDF,
      0xE1, 0x8B, 0xAD, 0x97, 0x19, 0x83, 0xA5, 0xCF,
      0xD1, 0xFB, 0x1D, 0x87, 0x09, 0xF3, 0x15, 0xBF,
      0xC1, 0x6B, 0x8D, 0x77, 0xF9, 0x63, 0x85, 0xAF,
      0xB1, 0xDB, 0xFD, 0x67, 0xE9, 0xD3, 0xF5, 0x9F,
      0xA1, 0x4B, 0x6D, 0x57, 0xD9, 0x43, 0x65, 0x8F,
      0x91, 0xBB, 0xDD, 0x47, 0xC9, 0xB3, 0xD5, 0x7F,
      0x81, 0x2B, 0x4D, 0x37, 0xB9, 0x23, 0x45, 0x6F,
      0x71, 0x9B, 0xBD, 0x27, 0xA9, 0x93, 0xB5, 0x5F,
      0x61, 0x0B, 0x2D, 0x17, 0x99, 0x03, 0x25, 0x4F,
      0x51, 0x7B, 0x9D, 0x07, 0x89, 0x73, 0x95, 0x3F,
      0x41, 0xEB, 0x0D, 0xF7, 0x79, 0xE3, 0x05, 0x2F,
      0x31, 0x5B, 0x7D, 0xE7, 0x69, 0x53, 0x75, 0x1F,
      0x21, 0xCB, 0xED, 0xD7, 0x59, 0xC3, 0xE5, 0x0F,
      0x11, 0x3B, 0x5D, 0xC7, 0x49, 0x33, 0x55, 0xFF ]

    k = 0x47
    for each byte P[i]:
      C[i] = 0x2F + P[i] * INV8[k >> 1]
      k = (2 * P[i] + 1) AND 0xFF
    
A message block can contain multiple commands separated by a newline. Known
command types are listed below.

#### ID (`ID`)

Format: `ID X`

ID of the message. Purpose is unknown.

#### Set colour (`CL`)

Format: `CL 0xLLRRGGBBh`

Set the colour of an LED. `LL` is the LED; `RR`, `GG`, `BB` are the red, green,
and blue components.

|LED|Position|
|---|--------|
|0  |Bottom  |
|1  |Left    |
|2  |Middle  |
|3  |Right   |
|4  |Nose    |

#### Set palette (`PL`)

Format: `PL 0X`

`X` is from 0 to 7. This sets palette colour for choreography files.

#### Play choreography (`CH`)

Format: `CH url`.

Retrieves a choreography file from the given URL and plays it.

#### Play music (`MU`, `MC`)

Format: `MU url`.

Plays sound from the given URL.

`MC` is a synonym.

#### Play Shoutcast stream (`ST`)

Format: `ST url`.

Plays Shoutcast streaming radio from given URL.

#### Wait (`MW`)

Format: `MW`

Takes no parameters. Forces Nabaztag to finish given commands (e.g. playing
sound) before proceeding to the next one.

## Choreographies

Choreographies are files that can be played by calling them from message blocks
with CH command. Choreographies contains led control and ear control. Each of
the five leds can be controlled to have RGB values from 0-255. Ears can be
moved to certain positions from `0x0` to `0x12` forwards or backwards. Also
there is a possibility to move an ear forwards a certain step count. These
files are made from three parts. First there is a header which contains the
length of bytes and in middle there is the data and at the end four zero bytes.
Here we have example of simple choreography file where the data part is in
orange colour. This data contains one command that tells the center led to
change into red.

    00 00 00 08 00 07 02 FF 00 00 00 00 00 00 00 00

Data is filled with smaller command byte arrays. Choreography files are timed
and this reflects also to the command bytes. First byte of command bytes tells
how long to wait before playing a new command from the beginning of last
command. Second byte tells the type of command. After these two bytes there can
be any amount of bytes relative to the command. In this example we have one
command where the first byte on white background tells that the command is
played instantly when the file is started to play. Next on blue background we
have the type of command byte and in the end with orange colour we have the data
of command.

    00 07 02 FF 00 00 00 00

### List of possible commands:

Tempo command sets how fast commands are played. Type byte is 1. Data part is
build from one byte that tells tempo. At default this value is 10. Below is
example that sets tempo to 15.

    00 01 0E

Led command sets colour of given led to RGB colour. Type byte is 7 and data is
build following way: first byte tells led which colour to change from 0 to 4,
next led is red value followed by green and blue bytes. Lastly there is two
zero bytes that are being ignored. Here in below we have example which sets
center led red value to 255, green to 255 and blue to zero.

    00 07 02 FF FF 00 00 00

Ear command type byte is 8 and it consist of three data bytes. First one is the
ear that can be 0 (right) or 1 (left). Next is the ear position between `0x0`
and `0x12` (0-18). And third byte is the direction of rotation, 0 (forward) or
1 (backward). In the example, right ear is rotated 'forward up' position by
rotating the ear backwards.

    00 08 00 05 01

Play random MIDI command is type `0x10` command. Plays random mini note. No
data part.

    00 10

Ear step command is type `0x11` command. Data is made from two bytes: first
byte selects the ear 0 (right) or 1 (left) and next byte tells how many steps
to go forwards. In the example we move left ear ten steps forwards.

    00 11 01 0A

Addition to these commands there are "ifne" command that is some sort of loop
command and "attend" command that synchronizes choreography to sound. There two
commands has not been tested and their functionality isn't sure yet.

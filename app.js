'use strict';
var express = require('express');
var fs = require('fs');
var block = require('./lib/block');
var packet = require('./lib/packet');
var sprintf = require('./lib/sprintf');

var app = express();
var bunnies = {};
var port = 8111;

app.use(express.logger());
app.use(express['static'](__dirname + '/public'));
app.use(express.errorHandler({showStack: true, dumpExceptions: true}));

// Bootcode
app.get('/vl/bc.jsp', function(req, res){
  res.sendfile(__dirname + '/public/bootcode.bin');
});

// Tell Nabaztag the server for ping and broadcast
app.get('/vl/locate.jsp', function(req, res){
  var sn = req.params.sn;
  bunnies[sn] = [];
  bunnies[sn].push(block.ping(5));
  bunnies[sn].push(block.message(sprintf('ID 0\nMU http://%s:%d/monkey.mp3', req.host, port)));
  res.send(sprintf('ping %s:%d\nbroad %s:%d',
                   req.host, port, req.host, port));
});

// Called when Nabaztag pings server
app.get('/vl/p4.jsp', function(req, res){
  var sn = req.params.sn;
  var blk;
  if (!bunnies[sn]) {
    blk = block.reboot();
  } else if (bunnies[sn].length > 0) {
    blk = bunnies[sn].shift();
  } else {
    blk = block.ambient(1);
  }
  res.send(packet.build([blk]));
});

// Hold down the button and speak
app.post('/vl/record.jsp', function(req, res){
  var stream = fs.createWriteStream('recording');
  req.pipe(stream);
  
  req.on('end', function(){
    res.send('');
  });
});

app.listen(port);
